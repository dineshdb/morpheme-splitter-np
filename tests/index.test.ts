import { separateMorphemes, isVowel, isConsonant, toIndependentVowel } from "../src/index"

test("Separates morphemes", function () {
    expect(separateMorphemes("विद्यार्थी")).toEqual(['व्', 'ि', 'द्', 'य्', 'ा', 'र्', 'थ्', 'ी']);
    expect(separateMorphemes("गत")).toEqual(["ग्", "अ", "त्", "अ"]);
    expect(separateMorphemes("गई")).toEqual(["ग्", "ई"]);
})

test("Tests vowels", function () {
    expect(isVowel("ं")).toEqual(true)
    expect(isVowel("ः")).toEqual(true)
    expect(isVowel("ऄ")).toEqual(true)
    expect(isVowel("अ")).toEqual(true)
    expect(isVowel("आ")).toEqual(true)
    expect(isVowel("इ")).toEqual(true)
    expect(isVowel("ई")).toEqual(true)
    expect(isVowel("उ")).toEqual(true)
})

test("To Independent Vowels", function () {
    expect(toIndependentVowel("ा")).toEqual("आ")
    expect(toIndependentVowel("ै")).toEqual("ऐ")
    expect(toIndependentVowel("ग")).toEqual("ग")
})