export const consonants = new RegExp("[\u0915-\u0939]")
export const halanta = "्"
export const vowels = new RegExp("[\u0904-\u0914\u093A-\u094C\u0902\u0903]")
export const dependentVowels = new RegExp("[\u093A-\u094C]")

export const isVowel = letter => letter.match(vowels) !== null
export const isConsonant = letter => letter.match(consonants) !== null
export const isHalanta = letter => letter === halanta

// Converts the dependent vowels to independent vowels. Assumes correct input is given.
export function toIndependentVowel(vowel: string) : string {
	return vowel.match(dependentVowels) !== null ? String.fromCharCode(vowel.charCodeAt(0)-0x093E+0x0906) : vowel
}

export function separateMorphemes(word:  string) : Array<string> {
	let morphemes = []
	for (let i = 0; i < word.length; i++) {
		if (isVowel(word[i])) {
			morphemes.push(word[i])
		} else if (isConsonant(word[i])) {
			// Last letter
			if (i === word.length - 1) {
				morphemes.push(word[i] + halanta)
				morphemes.push("अ")
			} else if (isHalanta(word[i + 1])) {
				//  क + ् in Unicode representation translates to क्, a single morpheme in Nepali.
				morphemes.push(word[i] + halanta)
				// Skip processing next letter i.e. halanta
				i++
			} else if (isConsonant(word[i + 1])) {
				morphemes.push(word[i] + halanta)
				morphemes.push("अ")
			} else if (isVowel(word[i + 1])) {
				morphemes.push(word[i] + halanta)
			}
		}
	}
	return morphemes
}

export default separateMorphemes